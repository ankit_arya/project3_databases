xquery version "3.0";
(:
CSE532
File name: query5.xq
Author: Ankit Arya(107017927)

Query5: Find all chained co-auditions. A chained co-auditions is the transitive closure of the following
binary relation: X and Y (directly) co-auditioned iff they both performed the same piece in the
same show and got the same score from at least one same judge. Thus, a chained co-audition
can be either a direct or an indirect co-audition 

I pledge my honor that all parts of this project were done by me alone and
without collaboration with anybody else.

:)

declare namespace functx = "http://www.functx.com";
declare function functx:value-union
  ( $arg1 as xs:anyAtomicType* ,
    $arg2 as xs:anyAtomicType* )  as xs:anyAtomicType* {
  distinct-values(($arg1, $arg2))
 };


declare function local:SamePiece() as xs:string+{
    for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
        $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
    where $A1/artworkID =$A2/artworkID and $A1/showID=$A2/showID and $A1/contestantID != $A2/contestantID
    return for $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores,
               $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
           where $A1/AuditionID = $S1/auditionID and $A2/AuditionID = $S2/auditionID 
                and $S1/judgeID = $S2/judgeID and $S1/score   = $S2/score
            return for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
                    $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
                    where $C1/ContestantID= $A1/contestantID and $C2/ContestantID= $A2/contestantID
                    return <result>{$C1/name||','||$C2/name}</result>
};



let $result := (local:SamePiece())
let $list := (local:SamePiece())
let $res:=functx:value-union($result,for $pair1 in (for $i in $list
                                                    let $t:=tokenize($i, ",")
                                                    return <tuple><i>{ $t[1] }</i><j>{ $t[2] }</j></tuple>)
                                     for $pair2 in (for $i in $list
                                                    let $t:=tokenize($i, ",")
                                                    return <tuple><i>{ $t[1] }</i><j>{ $t[2] }</j></tuple>)
                                     where $pair1/j=$pair2/i and $pair1/i !=$pair2/j
                                     return <result>{$pair1/i||', '|| $pair2/j}</result>)
return  for $pair1 in( for $n1 in $res
                       let $name:=tokenize($n1, ",")
                       return <tuple><i>{ $name[1] }</i><j>{ $name[2] }</j></tuple>)
        for $pair2 in( for $n2 in $res
                       let $name:=tokenize($n2, ",")
                       return <tuple><i>{ $name[1] }</i><j>{ $name[2] }</j></tuple>)
        where $pair1/j=$pair2/i and $pair1/i > $pair2/j
        return $pair1/i||', '|| $pair2/j
