xquery version "3.0";
(:
CSE532
File name: query2.xq
Author: Ankit Arya(107017927)

Query 2: Find all pairs of contestants who happened to audition the same piece (in possibly different
shows) and got the same average score for that piece.

I pledge my honor that all parts of this project were done by me alone and
without collaboration with anybody else.
:)
 
 
for $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
where $C1/ContestantID>$C2/ContestantID
      and (for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
               $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
           where $A1/AuditionID =$S1/auditionID 
                 and $C1/ContestantID= $A1/contestantID
                 and $A1/artworkID=$Art/ArtworkID
           group by $d:= $C1/name
           return for $Sh1 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
                  where $Sh1/ShowID=$A1/showID
                  return <result name="{$d}">"{avg($S1/score)}"</result>)
       =
       (for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
        where $A2/AuditionID =$S2/auditionID 
             and $C2/ContestantID= $A2/contestantID
             and $A2/artworkID=$Art/ArtworkID
       group by $d:= $C2/name
       return for $Sh2 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
          where $Sh2/ShowID=$A2/showID
          return <result name="{$d}">"{avg($S2/score)}"</result>)
return $C1/name ||' '|| $C2/name

