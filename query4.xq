xquery version "3.0";
(:
CSE532
File name: query4.xq

Author: Ankit Arya(107017927)
  
Query 4: Find all pairs of contestants such that the first contestants has  performed all the pieces of the second contestant (possibly in different shows)

I pledge my honor that all parts of this project were done by me alone and
without collaboration with anybody else.

:)
              
for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
where $C1/ContestantID != $C2/ContestantID 
return ($C1/name || ' '||$C2/name) 
       [not(.=(for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
                   $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
               where $C1/ContestantID=$A1/contestantID
                     and $A1/artworkID=$Art/ArtworkID
                     and $A1/artworkID [not(.=(for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
                                                where $C2/ContestantID=$A2/contestantID
                                                return $A2/artworkID))] 
               return ($C1/name || ' '||$C2/name)))]
