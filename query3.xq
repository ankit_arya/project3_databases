xquery version "3.0";
(:
CSE532
File name: query3.xq

Author: Ankit Arya(107017927)
Query3: Find all pairs of contestants who auditioned the same piece in (possibly different) 
shows that had at least 3 judges and the two contestants got the same highest score.

I pledge my honor that all parts of this project were done by me alone and
without collaboration with anybody else.

 :)

for $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
where $C1/ContestantID>$C2/ContestantID
      and (for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A1/AuditionID =$S1/auditionID 
             and $C1/ContestantID= $A1/contestantID
             and $A1/artworkID=$Art/ArtworkID
       group by $d:= $C1/name
       return for $Sh1 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
              where $Sh1/ShowID=$A1/showID
                    and count($S1/judgeID) >2
              return <result name="{$d}">"{max($S1/score)}"</result>)
       =
       (for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
            $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
        where $A2/AuditionID =$S2/auditionID 
              and $C2/ContestantID= $A2/contestantID
              and $A2/artworkID=$Art/ArtworkID
        group by $d:= $C2/name
        return for $Sh2 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
              where $Sh2/ShowID=$A2/showID
                    and count($S2/judgeID) >2
              return <result name="{$d}">"{max($S2/score)}"</result>)
return $C1/name ||' '|| $C2/name
