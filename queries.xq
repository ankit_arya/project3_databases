on "3.0";

(: Query1 -Find all pairs of contestants who happened to audition the same piece during the same show and got the same score from at least one judge :)
#Query 1
for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
    $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
where $A1/artworkID =$A2/artworkID 
      and $A1/showID=$A2/showID 
      and $A1/contestantID > $A2/contestantID
return for $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores,
           $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A1/AuditionID = $S1/auditionID
             and $A2/AuditionID = $S2/auditionID
             and $S1/judgeID = $S2/judgeID
             and $S1/score   = $S2/score
        return for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
                $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
                where $C1/ContestantID= $A1/contestantID
                      and $C2/ContestantID= $A2/contestantID
                return $C1/name || ' '||$C2/name


(: Query 2
# All pairs of contestants who hapened to audition the same piece
# (in possibly different
# shows) and got the same average score for that piece.
:)
for $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
where $C1/ContestantID>$C2/ContestantID
      and (for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A1/AuditionID =$S1/auditionID 
             and $C1/ContestantID= $A1/contestantID
             and $A1/artworkID=$Art/ArtworkID
       group by $d:= $C1/name
       return for $Sh1 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
          where $Sh1/ShowID=$A1/showID
          return <result name="{$d}">"{avg($S1/score)}"</result>)
       =
       (for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A2/AuditionID =$S2/auditionID 
             and $C2/ContestantID= $A2/contestantID
             and $A2/artworkID=$Art/ArtworkID
       group by $d:= $C2/name
       return for $Sh2 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
          where $Sh2/ShowID=$A2/showID
          return <result name="{$d}">"{avg($S2/score)}"</result>)
return $C1/name ||' '|| $C2/name

(: Query 3
Find all pairs of contestants who auditioned the same piece in (possibly different) shows that had at least 3 judges and the two contestants got the same highest score. :)

for $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
where $C1/ContestantID>$C2/ContestantID
      and (for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A1/AuditionID =$S1/auditionID 
             and $C1/ContestantID= $A1/contestantID
             and $A1/artworkID=$Art/ArtworkID
       group by $d:= $C1/name
       return for $Sh1 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
              where $Sh1/ShowID=$A1/showID
                    and count($S1/judgeID) >2
              return <result name="{$d}">"{max($S1/score)}"</result>)
       =
       (for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
           $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A2/AuditionID =$S2/auditionID 
             and $C2/ContestantID= $A2/contestantID
             and $A2/artworkID=$Art/ArtworkID
       group by $d:= $C2/name
       return for $Sh2 in doc("/db/apps/talentshow.xml")/TalentShow/Shows
          where $Sh2/ShowID=$A2/showID
                and count($S2/judgeID) >2
          return <result name="{$d}">"{max($S2/score)}"</result>)
return $C1/name ||' '|| $C2/name



(: query 4
# Find all pairs of contestants such that the first contestants has  performed all the pieces of the second contestant (possibly in different shows)
:)
              
for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
    $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
where $C1/ContestantID != $C2/ContestantID 
return ($C1/name || ' '||$C2/name) 
       [not(.=(for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
                   $Art in doc("/db/apps/talentshow.xml")/TalentShow/Artworks
               where $C1/ContestantID=$A1/contestantID
                     and $A1/artworkID=$Art/ArtworkID
                     and $A1/artworkID [not(.=(for $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
                                                where $C2/ContestantID=$A2/contestantID
                                                return $A2/artworkID))] 
               return ($C1/name || ' '||$C2/name)))]


(:Find all chained co-auditions. A chained co-auditions is the transitive closure of the following binary relation: X and Y (directly) co-auditioned iff they both performed the same piece in the same show and got the same score from at least one (same) judge. Thus, a chained co-audition can be either a direct or an indirect co-audition. :)

declare namespace functx = "http://www.functx.com";
declare function functx:value-union
  ( $arg1 as xs:anyAtomicType* ,
    $arg2 as xs:anyAtomicType* )  as xs:anyAtomicType* {

  distinct-values(($arg1, $arg2))
 } ;


declare function local:SamePiece() as xs:string+{
    for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
        $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
    where $A1/artworkID =$A2/artworkID and $A1/showID=$A2/showID and $A1/contestantID != $A2/contestantID
    return for $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores,
               $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
           where $A1/AuditionID = $S1/auditionID and $A2/AuditionID = $S2/auditionID 
                and $S1/judgeID = $S2/judgeID and $S1/score   = $S2/score
            return for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
                    $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
                    where $C1/ContestantID= $A1/contestantID and $C2/ContestantID= $A2/contestantID
                    return <result>{$C1/name||','||$C2/name}</result>
};



let $result := (local:SamePiece())
let $list := (local:SamePiece())
let $res:=functx:value-union($result,for $pair1 in (for $i at $p1 in $list
                    let $t:=tokenize($i, ",")
                    return <tuple><i>{ $t[1] }</i><j>{ $t[2] }</j></tuple>)
             for $pair2 in (for $i at $p1 in $list
                    let $t:=tokenize($i, ",")
                    return <tuple><i>{ $t[1] }</i><j>{ $t[2] }</j></tuple>)
             where $pair1/j=$pair2/i and $pair1/i !=$pair2/j
            return <result>{$pair1/i||', '|| $pair2/j}</result>)
return  for $pair1 in ( for $n1 in $res
               let $name:=tokenize($n1, ",")
               return <tuple><i>{ $name[1] }</i><j>{ $name[2] }</j></tuple>)
        for $pair2 in( for $n2 in $res
                          let $name:=tokenize($n2, ",")
                          return <tuple><i>{ $name[1] }</i><j>{ $name[2] }</j></tuple>)
        where $pair1/j=$pair2/i and $pair1/i > $pair2/j
        return $pair1/i||', '|| $pair2/j

