(:
CSE532
File name: query1.xq

Author: Ankit Arya(107017927)

Query1 -Find all pairs of contestants who happened to audition the same piece during the same show and got the same score from at least one judge

I pledge my honor that all parts of this project were done by me alone and
without collaboration with anybody else.

:)

xquery version "3.0";
for $A1 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions,
    $A2 in doc("/db/apps/talentshow.xml")/TalentShow/Auditions
where $A1/artworkID =$A2/artworkID 
      and $A1/showID=$A2/showID 
      and $A1/contestantID > $A2/contestantID
return for $S1 in doc("/db/apps/talentshow.xml")/TalentShow/Scores,
           $S2 in doc("/db/apps/talentshow.xml")/TalentShow/Scores
       where $A1/AuditionID = $S1/auditionID
             and $A2/AuditionID = $S2/auditionID
             and $S1/judgeID = $S2/judgeID
             and $S1/score   = $S2/score
        return for $C1 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants,
                   $C2 in doc("/db/apps/talentshow.xml")/TalentShow/Contestants
               where $C1/ContestantID= $A1/contestantID
                     and $C2/ContestantID= $A2/contestantID
               return $C1/name || ' '||$C2/name


